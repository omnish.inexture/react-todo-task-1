import React from "react";

const MapList = ({ setList, list, ele }) => {
    const taskCompleted = (id) => {
        setList(
            list.map((ele) => {
                if (ele.id === id) {
                    return { ...ele, completed: !ele.completed };
                }
                return ele;
            }),
        );
    };

    const deleteTask = (delid) => {
        setList(
            list.filter((ele) => {
                return ele.id !== delid;
            }),
        );
    };

    return (
        <div
            className={`list_Container ${ele.completed ? "completed" : ""}`}
            key={ele.id}
        >
            <div className="todoTask">
                <li>{ele.input}</li>
            </div>

            <div className="icnoContainer">
                <i
                    onClick={() => taskCompleted(ele.id)}
                    className={`fas fa-${ele.completed ? "times" : "check"}`}
                ></i>
                <i
                    onClick={() => deleteTask(ele.id)}
                    className="fas fa-trash"
                ></i>
            </div>
        </div>
    );
};

export default MapList;
