const Input = ({ input, setInput, setList, list, select, setSelect }) => {
    const addTodo = () => {
        setList([
            ...list,
            { input, id: Math.random() * 99999999999, completed: false },
        ]);
        setInput("");
    };

    const selectVal = (e) => {
        setSelect(e.target.value);
    };

    return (
        <div className="input_container">
            <div className="inputBox">
                <input
                    type="text"
                    placeholder="To-Do"
                    value={input}
                    onChange={(e) => setInput(e.target.value)}
                />
                <i
                    onClick={input ? addTodo : ""}
                    className="fas fa-plus-square"
                ></i>
            </div>

            <div>
                <select name="option" id="option" onChange={selectVal}>
                    <option value="All">All</option>
                    <option value="Completed">Completed</option>
                    <option value="Pending">Pending</option>
                </select>
            </div>
        </div>
    );
};

export default Input;
