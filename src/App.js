import { useEffect, useState } from 'react';
import './App.css';
import Input from './Input';
import List from './List';

function App() {

  const [list, setList] = useState([])
  const [input, setInput] = useState("")
  const [select, setSelect] = useState("All");
  

  return (
    <div className="main_container">
      <Input list={list} setList={setList} input={input} setInput={setInput} select={select} setSelect={setSelect}/>
      <div>
        <List list={list} setList={setList} select={select} setSelect={setSelect}/>
      </div>
    </div>
  );
}

export default App;
