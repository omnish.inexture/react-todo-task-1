import React from "react";
import MapList from "./MapList";

const List = ({ setList, list, select, setSelect }) => {
    return (
        <>
            {select === "All"
                ? list.length > 0 &&
                  list.map((ele) => (
                      <MapList ele={ele} list={list} setList={setList} />
                  ))
                : ""}
            {select === "Pending"
                ? list.length > 0 &&
                  list.map((ele) =>
                      ele.completed ? (
                          ""
                      ) : (
                          <MapList ele={ele} list={list} setList={setList} />
                      ),
                  )
                : ""}

            {select === "Completed"
                ? list.length > 0 &&
                  list.map((ele) =>
                      ele.completed ? (
                          <MapList ele={ele} list={list} setList={setList} />
                      ) : (
                          ""
                      ),
                  )
                : ""}
        </>
    );
};

export default List;
